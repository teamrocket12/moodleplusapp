package com.cop.abhinav_kushagra.moodleplusapp;

import java.io.Serializable;


public class gradeSubclass implements Serializable {
    int weightage;
    int userId;
    String name;
    int outOf;
    int registeredCourseId;
    int score;
    int id;
    int abs_marks;

    gradeSubclass(int weightage, int userId,String name,int outOf,int registeredCourseId,int score,int id){
        this.weightage=weightage;
        this.name=name;
        this.userId=userId;
        this.outOf=outOf;
        this.registeredCourseId=registeredCourseId;
        this.score=score;
        this.id=id;
    }


}

package com.cop.abhinav_kushagra.moodleplusapp;

import java.io.Serializable;


public class User implements Serializable{
    public String firstName;
    public String lastName;
    public String id;
    public String entryNumber;
    String email;
    String username;
    int type;

    public User(String firstName, String lastName, String id, String entryNumber, String email, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.entryNumber = entryNumber;
        this.email = email;
        this.username = username;
    }
    // more fields to be added later on the basis of requirements

    public User(String firstName, String lastName, String id, String entryNumber, String email, String username, int type) {
        this.email = email;
        this.entryNumber = entryNumber;
        this.firstName = firstName;
        this.id = id;
        this.lastName = lastName;
        this.type = type;
        this.username = username;
    }

    public User(String firstName, String lastName, String id, String entryNumber, String email){
        this.firstName=firstName;
        this.lastName=lastName;
        this.id=id;
        this.entryNumber=entryNumber;
        this.email=email;
    }

}

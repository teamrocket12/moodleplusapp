package com.cop.abhinav_kushagra.moodleplusapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    Button login_button;
    EditText username,password;
    NetworkFunctions net=new NetworkFunctions();
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_button = (Button) findViewById(R.id.loginbutton);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        progress=new ProgressDialog(this);
        progress.setMessage("Logging...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    //function to validate login
    public void validate_login(View view) {

        progress.show();
        net.loginresponse(username.getText().toString().trim(), password.getText().toString().trim(), this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)  {
                Intent intent = new Intent(LoginActivity.this , CourseListActivity.class );
                Log.d("Login",response.toString());
                String lastname="",firstname="",entrynumber="",id="",email="",uusername="";
                try {
                    lastname=response.getJSONObject("user").getString("last_name");
                    firstname=response.getJSONObject("user").getString("first_name");
                    entrynumber=response.getJSONObject("user").getString("entry_no");
                    email=response.getJSONObject("user").getString("email");
                    id=response.getJSONObject("user").getString("id");
                    uusername=response.getJSONObject("user").getString("username");
                    intent.putExtra("UserData", new User(firstname, lastname, id, entrynumber, email,uusername,response.getJSONObject("user").getInt("type_")));

                    CheckBox checkBox=(CheckBox) findViewById(R.id.keepLoggedin);
                    if(checkBox.isChecked()){
                        SaveSharedPreference.setUserName(LoginActivity.this, username.getText().toString().trim());
                        SaveSharedPreference.setPassword(LoginActivity.this, password.getText().toString().trim());
                    }
                    progress.cancel();
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    progress.cancel();
                    Toast.makeText(LoginActivity.this, "Invalid Username or Password", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Login","error");
                progress.cancel();
                Toast.makeText(LoginActivity.this, "Network Error!!", Toast.LENGTH_LONG).show();
            }
        });

    }
}

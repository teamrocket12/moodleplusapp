package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CourseListActivity extends AppCompatActivity {

    static class CourseListViewHolder {
        TextView coursename;
        TextView coursecode;
        int pos; //to store the position of the item within the list
    }

    NetworkFunctions net=new NetworkFunctions();

    TextView navigation_name,navigation_entryno,navigation_email,semester;
    ListView courseList,drawerList;
    LinearLayout linearUser;

    String[] drawerItemstudent={"Grades","Notification","Logout"};
    String[] drawerItemProf={"Notification","Logout"};
    ArrayList<Courses> courseListFetched =new ArrayList<Courses>();
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        Intent intent=getIntent();
        user=(User) intent.getSerializableExtra("UserData");

        semester=(TextView) findViewById(R.id.semesterName);
        navigation_name=(TextView) findViewById(R.id.nav_username);
        navigation_entryno=(TextView) findViewById(R.id.nav_entryno);
        navigation_email=(TextView) findViewById(R.id.nav_email);
        courseList = (ListView) findViewById(R.id.courseList);
        drawerList = (ListView) findViewById(R.id.drawerList);
        linearUser = (LinearLayout) findViewById(R.id.linearUser);

        navigation_name.setText(user.firstName + " " + user.lastName);
        navigation_entryno.setText(user.entryNumber);
        navigation_email.setText(user.email);
        courseListFetched.add(new Courses("", "Fetching"));

        linearUser.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourseListActivity.this, UserActivity.class);
                intent.putExtra("userData", user);
                startActivity(intent);

            }
        });

        if (user.type==0){
            drawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,drawerItemstudent));
            drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(CourseListActivity.this, GradesActivity.class);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(CourseListActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(CourseListActivity.this, LoginActivity.class);
                            logout();
                            // check again
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            break;
                    }

                }
            });
        }else{
            drawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,drawerItemProf));
            drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(CourseListActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(CourseListActivity.this, LoginActivity.class);
                            logout();
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            break;
                    }

                }
            });
        }

        courseList.setAdapter(new BaseAdapter() {

            LayoutInflater inflater = LayoutInflater.from(CourseListActivity.this);

            @Override
            public int getCount() {
                return courseListFetched.size();
            }

            @Override
            public Object getItem(int position) {
                return courseListFetched.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                CourseListViewHolder viewHolder;
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.courselist_item_layout, parent, false);
                    viewHolder = new CourseListViewHolder();
                    viewHolder.coursename = (TextView) convertView.findViewById(R.id.courseName);
                    viewHolder.coursecode = (TextView) convertView.findViewById(R.id.courseCode);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (CourseListViewHolder) convertView.getTag();
                }
                //String currentListData = getItem(position);
                viewHolder.coursename.setText(courseListFetched.get(position).name);
                viewHolder.coursecode.setText(courseListFetched.get(position).courseCode.toUpperCase());
                viewHolder.pos = position;
                return convertView;
            }
        });

        courseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(CourseListActivity.this, CourseActivity.class);
                intent.putExtra("CourseCode", courseListFetched);
                intent.putExtra("position",position);
                intent.putExtra("size",courseListFetched.size());
                intent.putExtra("Userdata",user);
                startActivity(intent);

            }
        });

        populateCourseList();

    }

    // function to get all courses
    public void populateCourseList(){

        net.populateCourses(
                this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)  {
                courseListFetched=new ArrayList<Courses>();
                Log.d("CousreList_response",response.toString());
                try {
                    JSONArray courses=response.getJSONArray("courses");
                    semester.setText("Semester "+response.getString("current_sem"));
                    int i=0;
                    while(i<courses.length()){
                        courseListFetched.add(new Courses(courses.getJSONObject(i).getString("code"),
                                courses.getJSONObject(i).getInt("credits"),
                                        courses.getJSONObject(i).getString("description"),
                                                courses.getJSONObject(i).getInt("id"),
                                                        courses.getJSONObject(i).getString("l_t_p"),
                                                                courses.getJSONObject(i).getString("name")));
                        i++;
                    }
                    ((BaseAdapter) courseList.getAdapter()).notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CourseListActivity.this, "Network Error!!", Toast.LENGTH_LONG).show();
                Log.d("Login","error");
            }
        });
    }

    public void logout(){

        SaveSharedPreference.clear(CourseListActivity.this);
        net.logoutresponse(
                this, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("Logout_response", response.toString());

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CourseListActivity.this, "Network Error", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }
}

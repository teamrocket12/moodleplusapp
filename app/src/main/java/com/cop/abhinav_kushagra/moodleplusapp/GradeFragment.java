package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class GradeFragment extends Fragment {

    View rootview;
    ListView gradeList;
    ArrayList<gradeSubclass> grade = new ArrayList<gradeSubclass>();
    NetworkFunctions net= new NetworkFunctions();
    BaseAdapter adapter;
    String coursecode="";



    static class GradeListViewHolder {
        TextView gradeitem,score,weight,absMarks;
        int pos; //to store the position of the item within the list
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg=getArguments();
        Courses course=(Courses)arg.getSerializable("CourseInfo");
        coursecode=((Courses)arg.getSerializable("CourseInfo")).courseCode;
        getGrade();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview= inflater.inflate(R.layout.fragment_grade, container, false);
        gradeList = (ListView) rootview.findViewById(R.id.gradeListView);
        adapter=new BaseAdapter() {

            LayoutInflater inflater = LayoutInflater.from(getActivity());

            @Override
            public int getCount() {
                return grade.size()+1;
            }

            @Override
            public Object getItem(int position) {
                return grade.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                GradeListViewHolder viewHolder;
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.layout_grade_listview, parent, false);
                    viewHolder = new GradeListViewHolder();
                    viewHolder.gradeitem = (TextView) convertView.findViewById(R.id.gradeitem);
                    viewHolder.score = (TextView) convertView.findViewById(R.id.score);
                    viewHolder.weight = (TextView) convertView.findViewById(R.id.weight);
                    viewHolder.absMarks = (TextView) convertView.findViewById(R.id.absMarks);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (GradeListViewHolder) convertView.getTag();
                }
                //String currentListData = getItem(position);
                Log.d("grade_position",position+"");
                if (position == 0) {
                    viewHolder.gradeitem.setText("Grade Item");
                    viewHolder.score.setText("Score");
                    viewHolder.weight.setText("Weight");
                    viewHolder.absMarks.setText("Marks");
                } else if(position==grade.size()){
                    Log.d("grade_position",position+"");
                    viewHolder.gradeitem.setText("Total");
                    viewHolder.score.setText("");
                    viewHolder.weight.setText("");
                    viewHolder.absMarks.setText(grade.get(position-1).abs_marks + "");

                }else{
                    Log.d("Grade", grade.get(position-1).score + "");
                    viewHolder.gradeitem.setText(grade.get(position-1).name);
                    viewHolder.score.setText(grade.get(position-1).score + "");
                    viewHolder.weight.setText(grade.get(position-1).weightage + "");
                    viewHolder.absMarks.setText(grade.get(position-1).abs_marks + "");


                }

                viewHolder.pos = position;
                return convertView;
            }
        };

        gradeList.setAdapter(adapter);

        return rootview;
    }

    //function to get grade in a course
    public void getGrade() {

        net.getGrade(
                coursecode, getActivity(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Grades_response", response.toString());
                        try {
                            JSONArray gradesJSON = response.getJSONArray("grades");

                            int i = 0;
                            while (i < gradesJSON.length()) {
                                gradeSubclass tempGrades = new gradeSubclass(gradesJSON.getJSONObject(i).getInt("weightage"), gradesJSON.getJSONObject(i).getInt("user_id"), gradesJSON.getJSONObject(i).getString("name"), gradesJSON.getJSONObject(i).getInt("out_of"), gradesJSON.getJSONObject(i).getInt("registered_course_id"), gradesJSON.getJSONObject(i).getInt("score"), gradesJSON.getJSONObject(i).getInt("id"));
                                tempGrades.abs_marks = (tempGrades.score * tempGrades.weightage) / tempGrades.outOf;
                                grade.add(tempGrades);
                                i++;
                            }
                            int sum=0;
                            for (int j=0;j<grade.size();j++){
                                sum=sum+grade.get(j).abs_marks;
                            }
                            gradeSubclass temp = new gradeSubclass(0,0,"Total",0,0,0,0);
                            temp.abs_marks=sum;
                            grade.add(temp);
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network Error!!", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }

}

package com.cop.abhinav_kushagra.moodleplusapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GradesActivity extends ActionBarActivity {

    ArrayList<Grades> grades=new ArrayList<Grades>();
    NetworkFunctions net=new NetworkFunctions();
    CollectionPagerAdapter adapter;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle("Grades");
        adapter = new CollectionPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.graderPager);
        viewPager.setAdapter(adapter);
        getGrade();

    }
    public class CollectionPagerAdapter extends FragmentStatePagerAdapter {
        public CollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new ObjectFragment();
            Bundle args = new Bundle();
            args.putSerializable("grade", grades.get(i).grade);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return grades.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return grades.get(position).course.courseCode.toUpperCase()+":"+grades.get(position).course.name;
        }
    }

    public static class ObjectFragment extends Fragment {
        ArrayList<gradeSubclass> grade = new ArrayList<gradeSubclass>();
        ListView gradeList;

        static class GradeListViewHolder {
            TextView gradeitem,score,weight,absMarks;
            int pos; //to store the position of the item within the list
        }
        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(
                    R.layout.fragment_grades, container, false);
            Bundle args = getArguments();
            grade=(ArrayList<gradeSubclass>)args.getSerializable("grade");
            gradeList=(ListView) rootView.findViewById(R.id.gradeList);
            gradeList.setAdapter(new BaseAdapter() {

                LayoutInflater inflater = LayoutInflater.from(getActivity());

                @Override
                public int getCount() {
                    return (grade.size() + 1);
                }

                @Override
                public Object getItem(int position) {
                    if (position == 0) {
                        return grade.get(position);
                    } else {
                        return grade.get(position - 1);
                    }
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    GradeListViewHolder viewHolder;
                    if (convertView == null) {
                        convertView = inflater.inflate(R.layout.layout_grade_listview, parent, false);
                        viewHolder = new GradeListViewHolder();
                        viewHolder.gradeitem = (TextView) convertView.findViewById(R.id.gradeitem);
                        viewHolder.score = (TextView) convertView.findViewById(R.id.score);
                        viewHolder.weight = (TextView) convertView.findViewById(R.id.weight);
                        viewHolder.absMarks = (TextView) convertView.findViewById(R.id.absMarks);
                        convertView.setTag(viewHolder);
                    } else {
                        viewHolder = (GradeListViewHolder) convertView.getTag();
                    }
                    //String currentListData = getItem(position);
                    Log.d("grade_position",position+"");
                    if (position == 0) {
                        viewHolder.gradeitem.setText("Grade Item");
                        viewHolder.score.setText("Score");
                        viewHolder.weight.setText("Weight");
                        viewHolder.absMarks.setText("Marks");
                    } else if(position==grade.size()){
                        Log.d("grade_position",position+"");
                        viewHolder.gradeitem.setText("Total");
                        viewHolder.score.setText("");
                        viewHolder.weight.setText("");
                        viewHolder.absMarks.setText(grade.get(position-1).abs_marks + "");

                    }else{
                            Log.d("Grade", grade.get(position-1).score + "");
                            viewHolder.gradeitem.setText(grade.get(position-1).name);
                            viewHolder.score.setText(grade.get(position-1).score + "");
                            viewHolder.weight.setText(grade.get(position-1).weightage + "");
                            viewHolder.absMarks.setText(grade.get(position-1).abs_marks + "");


                    }

                    viewHolder.pos = position;
                    return convertView;
                }
            });


            return rootView;
        }
    }
    // function to get grade of all course
    public void getGrade(){

        net.getAllGrades(
                this, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Grades_response", response.toString());
                        try {
                            JSONArray gradesJSON = response.getJSONArray("grades");
                            JSONArray courseJSON = response.getJSONArray("courses");
                            ArrayList<String> tempcourse=new ArrayList<String>();

                            int i = 0;
                            while (i < gradesJSON.length()) {
                                if(tempcourse.contains(courseJSON.getJSONObject(i).getString("code"))){
                                    gradeSubclass tempGrades = new gradeSubclass(gradesJSON.getJSONObject(i).getInt("weightage"), gradesJSON.getJSONObject(i).getInt("user_id"), gradesJSON.getJSONObject(i).getString("name"), gradesJSON.getJSONObject(i).getInt("out_of"), gradesJSON.getJSONObject(i).getInt("registered_course_id"), gradesJSON.getJSONObject(i).getInt("score"), gradesJSON.getJSONObject(i).getInt("id"));
                                    tempGrades.abs_marks=(tempGrades.score*tempGrades.weightage)/tempGrades.outOf;
                                    grades.get(tempcourse.indexOf(courseJSON.getJSONObject(i).getString("code"))).grade.add(tempGrades);
                                }
                                else {
                                    tempcourse.add(courseJSON.getJSONObject(i).getString("code"));
                                    Courses tempCourse = new Courses(courseJSON.getJSONObject(i).getString("code"), courseJSON.getJSONObject(i).getString("name"));
                                    gradeSubclass tempGrades = new gradeSubclass(gradesJSON.getJSONObject(i).getInt("weightage"), gradesJSON.getJSONObject(i).getInt("user_id"), gradesJSON.getJSONObject(i).getString("name"), gradesJSON.getJSONObject(i).getInt("out_of"), gradesJSON.getJSONObject(i).getInt("registered_course_id"), gradesJSON.getJSONObject(i).getInt("score"), gradesJSON.getJSONObject(i).getInt("id"));
                                    tempGrades.abs_marks=(tempGrades.score*tempGrades.weightage)/tempGrades.outOf;
                                    grades.add(new Grades(tempCourse, tempGrades));
                                }
                                i++;
                            }

                            for (int k=0;k<grades.size();k++){
                                int sum=0;
                                for (int j=0;j<grades.get(k).grade.size();j++){
                                    sum=sum+grades.get(k).grade.get(j).abs_marks;
                                }
                                gradeSubclass temp = new gradeSubclass(0,0,"Total",0,0,0,0);
                                temp.abs_marks=sum;
                                grades.get(k).grade.add(temp);
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(GradesActivity.this, "Network Error!!", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }
}

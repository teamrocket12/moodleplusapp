package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    ArrayList<Notification> notifications=new ArrayList<Notification>();
    ArrayAdapter<Notification> notificationAdapter;
    NetworkFunctions net=new NetworkFunctions();
    ListView notificationListview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setTitle(R.string.notification);
        notificationListview=(ListView) findViewById(R.id.notificationList);
        notificationAdapter=new ArrayAdapter<Notification>(this,android.R.layout.simple_list_item_1,notifications){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView=(TextView) super.getView(position, convertView, parent);
                textView.setText(notifications.get(position).message);
                if(notifications.get(position).isSeen)
                    textView.setTextColor(getResources().getColor(R.color.level200));
                else
                    textView.setTextColor(getResources().getColor(R.color.level800));
                return textView;
            }

            @Override
            public int getCount() {
                return notifications.size();
            }

            @Override
            public Notification getItem(int position) {
                return notifications.get(position);
            }
        };
        notificationListview.setAdapter(notificationAdapter);
        notificationListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                notifications.get(position).isSeen = true;
                Intent intent = new Intent(NotificationActivity.this, DiscussionActivity.class);
                intent.putExtra("threadID", notifications.get(position).threadID);
                startActivity(intent);
            }
        });
        populateNotificationList();

    }
    // function to fetch all notification
    public void populateNotificationList(){

        net.getNotifications(
                this, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        notifications = new ArrayList<Notification>();
                        Log.d("Notification_response", response.toString());
                        try {
                            JSONArray notificationsJSON = response.getJSONArray("notifications");
                            int i = 0;
                            while (i < notificationsJSON.length()) {
                                notifications.add(new Notification(notificationsJSON.getJSONObject(i).getInt("user_id"), notificationsJSON.getJSONObject(i).getString("description"), notificationsJSON.getJSONObject(i).getInt("is_seen"), notificationsJSON.getJSONObject(i).getString("created_at"), notificationsJSON.getJSONObject(i).getInt("id")));
                                i++;
                            }
                            Log.d("Notifiaction_response", notifications.size() + " ");
                            if(notifications.size()>0){
                                Log.d("Notification_message",notifications.get(0).message);
                            }
                            notificationAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(NotificationActivity.this, "Network error", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }
}


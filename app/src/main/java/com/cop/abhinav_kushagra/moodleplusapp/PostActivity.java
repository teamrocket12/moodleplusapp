package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class PostActivity extends AppCompatActivity {
    NetworkFunctions net=new NetworkFunctions();
    EditText titleentry,descriptionentry;


    String courseCode="csl838";
    String success="false";
    int threadid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        setTitle("Post");

        Intent intent=getIntent();
        courseCode=((Courses)intent.getSerializableExtra("CourseDetails")).courseCode;
        titleentry=(EditText) findViewById(R.id.newThreadTitle);
        descriptionentry=(EditText) findViewById(R.id.newThreadDescription);
    }

    //executed when post button is pressed
    public void postNewThread(View view){
        Log.d("sd", titleentry.getText().toString().trim());
        net.postThreads(titleentry.getText().toString().trim(), descriptionentry.getText().toString().trim(), courseCode,
                this, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("NewThread_response", response.toString());
                        try {
                            JSONObject responseJSON= new JSONObject(response);
                            success = responseJSON.getString("success");
                            if (success.equals("true")) {
                                threadid = responseJSON.getInt("thread_id");
                                Intent intent = new Intent(PostActivity.this,DiscussionActivity.class);
                                intent.putExtra("threadID",threadid);
                                startActivity(intent);

                                finish();
                            } else {
                                Toast.makeText(PostActivity.this, "Something went wrong!!", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(PostActivity.this, "Network error", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });



    }

}

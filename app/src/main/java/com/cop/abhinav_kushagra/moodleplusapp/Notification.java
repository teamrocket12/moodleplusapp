package com.cop.abhinav_kushagra.moodleplusapp;

import android.text.Html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Notification {
    boolean isSeen;
    int id;
    int userId;
    String dateTime;
    int threadID;
    String message;

    // function to parse notification message and to get the thread id
    void parseDescription(String description){
        this.message = description.replaceAll("\\<.*?>","");
        Pattern pattern = Pattern.compile("thread\\/[0-9]*");
        Matcher matcher = pattern.matcher(description);
        if (matcher.find())

        {
            String[] temp=matcher.group(0).split("/");
            if(temp.length>=2){

                this.threadID=(Integer.parseInt(temp[1]));
            }
        }

    }

    Notification(int userId,String description,int isSeen,String dateTime, int id){
            this.userId=userId;
            if(isSeen==1){
                this.isSeen=true;
            }
            else {
                this.isSeen=false;
            }
            this.dateTime=dateTime;
            this.id=id;
            parseDescription(description);
    }
}

package com.cop.abhinav_kushagra.moodleplusapp;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;


public class CourseActivity extends ActionBarActivity implements ActionBar.TabListener{


    TextView cc_actionbar,ltp_actionbar;
    ListView drawerList;
    LinearLayout userinfo;
    int size;

    Courses courses;
    ViewPager viewPager;
    ActionBar actionBar;
    TextView navigation_name,navigation_entryno,navigation_email;
    ArrayList<Courses> courseListFetched =new ArrayList<Courses>();
    NetworkFunctions net=new NetworkFunctions();
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        Intent intent = getIntent();
        courseListFetched=(ArrayList<Courses>) intent.getSerializableExtra("CourseCode");
        user=(User) intent.getSerializableExtra("Userdata");
        size = intent.getIntExtra("size",0);
        if(courseListFetched.size()==size ) {
            if (user.type == 0) {
                courseListFetched.add(new Courses("Grade", "extra1"));
            }
            courseListFetched.add(new Courses("Notification", "extra2"));
            courseListFetched.add(new Courses("Logout", "extra3"));
        }
        courses=courseListFetched.get(intent.getIntExtra("position",0));


        navigation_name=(TextView) findViewById(R.id.nav_username);
        navigation_entryno=(TextView) findViewById(R.id.nav_entryno);
        navigation_email=(TextView) findViewById(R.id.nav_email);
        drawerList = (ListView) findViewById(R.id.drawerList);
        viewPager = (ViewPager) findViewById(R.id.pager);
        userinfo = (LinearLayout) findViewById(R.id.linLayout);

        navigation_name.setText(user.firstName + " " + user.lastName);
        navigation_entryno.setText(user.entryNumber);
        navigation_email.setText(user.email);


        userinfo.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourseActivity.this, UserActivity.class);
                intent.putExtra("userData", user);
                startActivity(intent);
            }
        });
        drawerList.setAdapter(new ArrayAdapter<Courses>(this, android.R.layout.simple_list_item_1, courseListFetched) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView txtview = (TextView) super.getView(position, convertView, parent);
                txtview.setText(courseListFetched.get(position).courseCode.toUpperCase());
                return txtview;
            }

            @Override
            public int getCount() {
                return courseListFetched.size();
            }

            @Override
            public Courses getItem(int position) {
                return courseListFetched.get(position);
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if (courseListFetched.get(position).name.equals("extra1")) {
                    intent = new Intent(CourseActivity.this, GradesActivity.class);
                    startActivity(intent);

                } else if (courseListFetched.get(position).name.equals("extra2")) {
                    intent = new Intent(CourseActivity.this, NotificationActivity.class);
                    startActivity(intent);
                } else if (courseListFetched.get(position).name.equals("extra3")) {
                    intent = new Intent(CourseActivity.this, LoginActivity.class);
                    logout();
                    // check again
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    intent = new Intent(CourseActivity.this, CourseActivity.class);
                    intent.putExtra("CourseCode", courseListFetched);
                    intent.putExtra("position", position);
                    intent.putExtra("size",size);
                    intent.putExtra("Userdata", user);
                    startActivity(intent);
                }
            }
        });
        viewPager.setAdapter(new customFragmentAdapter(getSupportFragmentManager()));
        actionBar=getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setCustomView(R.layout.layout_tabview);
        cc_actionbar= (TextView) actionBar.getCustomView().findViewById(R.id.coursecode);
        ltp_actionbar = (TextView) actionBar.getCustomView().findViewById(R.id.ltp);
        cc_actionbar.setText(courses.courseCode.toUpperCase()+":"+courses.name);
        ltp_actionbar.setText(courses.ltp + ", Credits:" + courses.credits + "\n" + courses.description);
        actionBar.setDisplayShowCustomEnabled(true);
        // Create a tab listener that is called when the user changes tabs.
        actionBar.addTab(actionBar.newTab().setText("Assignment").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Forum").setTabListener(this));
        if(user.type==0){
            actionBar.addTab(actionBar.newTab().setText("Grade").setTabListener(this));
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
    }

    public class customFragmentAdapter extends FragmentPagerAdapter{

        public customFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            if (user.type==0)return 3;
                    else return 2;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle arg= new Bundle();
            arg.putSerializable("CourseInfo",courses);
            Fragment f;
            switch (position){
                case 0:
                    f=new AssignmentFragment();
                    f.setArguments(arg);
                    return f;
                case 1:
                    f=new ForumFragment();
                    f.setArguments(arg);
                    return f;
                case 2:
                    f=new GradeFragment();
                    f.setArguments(arg);
                    return f;
            }
            return null;
        }
    }
    public void logout(){


        SaveSharedPreference.clear(CourseActivity.this);

        net.logoutresponse(
                this, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("Logout_response", response.toString());

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CourseActivity.this, "Network Error", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }

}

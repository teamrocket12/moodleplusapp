package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class ForumFragment extends Fragment {

    NetworkFunctions net=new NetworkFunctions();
    ArrayList<DiscussionThread> threads=new ArrayList<DiscussionThread>();
    ArrayAdapter adapter;
    Courses course;
    ImageButton button;
    View rootview;
    ListView forumList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg=getArguments();
        course=((Courses)arg.getSerializable("CourseInfo"));
        String coursecode=course.courseCode;
        getThreads(coursecode);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_forum, container, false);
        forumList = (ListView) rootview.findViewById(R.id.forumList);
        button=(ImageButton) rootview.findViewById(R.id.newThread);
        button.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(),PostActivity.class);
                intent.putExtra("CourseDetails",course);
                startActivity(intent);// do something
            }
        });
        adapter=(new ArrayAdapter<DiscussionThread>(getActivity(),android.R.layout.simple_list_item_1,threads){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView =(TextView)super.getView(position, convertView, parent);
                textView.setText(threads.get(position).getTitle());
                return  textView;
            }

            @Override
            public int getCount() {
                return threads.size();
            }

            @Override
            public DiscussionThread getItem(int position) {
                return threads.get(position);
            }
        });
        forumList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),DiscussionActivity.class);
                intent.putExtra("threadID",threads.get(position).getId());
                startActivity(intent);
            }
        });

        forumList.setAdapter(adapter);
        return  rootview;
    }

    //function to get all thread in a course
    public void getThreads(final String courseCode){

        net.getThreads(courseCode,
                getActivity(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //notifications = new ArrayList<Notification>();
                        Log.d("Threads_response", response.toString());
                        try {
                            JSONArray threadsJSON = response.getJSONArray("course_threads");
                            int i = 0;
                            while (i < threadsJSON.length()) {
                                threads.add(new DiscussionThread(threadsJSON.getJSONObject(i).getInt("user_id"), threadsJSON.getJSONObject(i).getString("description"), threadsJSON.getJSONObject(i).getString("title"), threadsJSON.getJSONObject(i).getString("created_at"), threadsJSON.getJSONObject(i).getInt("registered_course_id"), threadsJSON.getJSONObject(i).getString("updated_at"), threadsJSON.getJSONObject(i).getInt("id")));
                                i++;
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network Error!!", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }






}

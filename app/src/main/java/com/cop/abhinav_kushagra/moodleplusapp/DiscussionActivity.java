package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Comment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DiscussionActivity extends AppCompatActivity {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    String timeNow=(dateFormat.format(date)); //2013/10/15 16:16:39
    Date deadlineDate;
    String success;
    static class CommentListViewHolder {
        TextView name,comment,time;
        int pos; //to store the position of the item within the list
    }

    int threadid;
    TextView title,description,comment;
    Button send;
    NetworkFunctions net=new NetworkFunctions();
    DiscussionThread thread;
    ArrayList<Comments> comments=new ArrayList<Comments>();
    ArrayList<String> users=new ArrayList<>();
    BaseAdapter adapter;
    ListView commentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Discussion Forum");
        setContentView(R.layout.activity_discussion);
        Intent intent=getIntent();
        threadid=intent.getIntExtra("threadID",1);
        comment=(TextView)findViewById(R.id.newComment);
        send=(Button) findViewById(R.id.send);
        title=(TextView)findViewById(R.id.threadTitle);
        description=(TextView)findViewById((R.id.threadDescription));


        commentList = (ListView) findViewById(R.id.comments);

        adapter = new BaseAdapter() {
            LayoutInflater inflater = LayoutInflater.from(DiscussionActivity.this);

            @Override
            public int getCount() {
                return comments.size();
            }

            @Override
            public Object getItem(int position) {
                return comments.size();
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                CommentListViewHolder viewHolder;
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.layout_comment, parent, false);
                    viewHolder = new CommentListViewHolder();
                    viewHolder.name = (TextView) convertView.findViewById(R.id.commentName);
                    viewHolder.comment = (TextView) convertView.findViewById(R.id.comment);
                    viewHolder.time = (TextView) convertView.findViewById(R.id.time);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (CommentListViewHolder) convertView.getTag();
                }
                //String currentListData = getItem(position);

                viewHolder.name.setText(comments.get(position).userName);
                viewHolder.comment.setText(comments.get(position).description);
                viewHolder.time.setText(comments.get(position).timediff);

                viewHolder.pos = position;
                return convertView;
            }
        };
        commentList.setAdapter(adapter);
        loadThread(threadid);

    }


    // function to get info of particular thread
    public void loadThread(int threadid){
        net.getThread(threadid,
                this, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        comments = new ArrayList<Comments>();
                        JSONObject threadsJSON;
                        JSONArray commentsJSON, usersJSON;
                        users = new ArrayList<String>();
                        try {
                            threadsJSON = response.getJSONObject("thread");
                            commentsJSON = response.getJSONArray("comments");
                            usersJSON = response.getJSONArray("comment_users");
                            thread = (new DiscussionThread(threadsJSON.getInt("user_id"), threadsJSON.getString("description"), threadsJSON.getString("title"), threadsJSON.getString("created_at"), threadsJSON.getInt("registered_course_id"), threadsJSON.getString("updated_at"), threadsJSON.getInt("id")));
                            int i = 0;
                            while (i < commentsJSON.length()) {

                                users.add(usersJSON.getJSONObject(i).getString("first_name") + " " + usersJSON.getJSONObject(i).getString("last_name"));
                                comments.add(new Comments(commentsJSON.getJSONObject(i).getInt("user_id"), commentsJSON.getJSONObject(i).getString("description"), commentsJSON.getJSONObject(i).getString("created_at"), users.get(i)));
                                try {
                                    deadlineDate = dateFormat.parse(commentsJSON.getJSONObject(i).getString("created_at"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long diff = (date.getTime() - deadlineDate.getTime());
                                long days = diff / (24 * 60 * 60 * 1000);
                                long hours = (diff % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000);
                                long min = (diff % (60 * 60 * 1000)) / (60 * 1000);
                                if (days > 0) {
                                    comments.get(i).timediff = commentsJSON.getJSONObject(i).getString("created_at");
                                } else if (hours > 0) {
                                    comments.get(i).timediff = hours + " hrs " + min + " min ago";
                                } else {
                                    comments.get(i).timediff = min + " min ago";
                                }
                                i++;
                            }
                            title.setText(thread.getTitle());
                            description.setText(thread.getDescription());

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("Thread_response", response.toString());


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DiscussionActivity.this, "Network Error", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }

    public void postComment(View view){
        //Log.d("sd", titleentry.getText().toString().trim());
        if(comment.getText().toString().trim().length()!=0) {
            net.postComments(thread.getId(), comment.getText().toString().trim(),
                    this, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //notifications = new ArrayList<Notification>();
                            Log.d("NewThread_response", response.toString());
                            try {
                                JSONObject responseJSON = new JSONObject(response);
                                JSONObject commentsJSON = responseJSON.getJSONObject("comment");

                                success = responseJSON.getString("success");
                                if (success.equals("true")) {
                                    Log.d("NewThread_debug", commentsJSON.toString());
                                    comments.add(new Comments(commentsJSON.getInt("user_id"), commentsJSON.getString("description"), commentsJSON.getString("created_at"), responseJSON.getString("user_name")));
                                    try {
                                        deadlineDate = dateFormat.parse(commentsJSON.getString("created_at"));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    date=new Date();
                                    long diff = (date.getTime() - deadlineDate.getTime());
                                    long days = diff / (24 * 60 * 60 * 1000);
                                    long hours = (diff % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000);
                                    long min = (diff % (60 * 60 * 1000)) / (60 * 1000);
                                    if (days > 0) {
                                        comments.get(comments.size() - 1).timediff = commentsJSON.getString("created_at");
                                    } else if (hours > 0) {
                                        comments.get(comments.size() - 1).timediff = hours + " hrs " + min + " min ago";
                                    } else {
                                        comments.get(comments.size() - 1).timediff = min + " min ago";
                                    }
                                    adapter.notifyDataSetChanged();


                                } else {
                                    //Log.d("NewThread_response_debug", commentsJSON.toString());
                                    Toast.makeText(DiscussionActivity.this, "Something went wrong!!", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                Log.d("NewThread_responses", response.toString());
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(DiscussionActivity.this, "Network Error!!", Toast.LENGTH_LONG).show();
                            Log.d("Login", "error");
                        }
                    });
        }

                comment.setText("");

    }

}

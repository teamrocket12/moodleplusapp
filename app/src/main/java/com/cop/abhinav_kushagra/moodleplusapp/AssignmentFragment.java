package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignmentFragment extends Fragment {
    NetworkFunctions net=new NetworkFunctions();
    ArrayList<Assignment> assignments=new ArrayList<Assignment>();
    View rootview;
    ListView assignmentList;
    ArrayAdapter adapter;


//    public AssignmentFragment() {
//        // Required empty public constructor
//    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg=getArguments();
        Courses course=(Courses)arg.getSerializable("CourseInfo");
        String coursecode=((Courses)arg.getSerializable("CourseInfo")).courseCode;
        getAssignment(coursecode);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview=inflater.inflate(R.layout.fragment_assignment, container, false);
        assignmentList = (ListView) rootview.findViewById(R.id.assignmentList);
       adapter=(new ArrayAdapter<Assignment>(getActivity(),android.R.layout.simple_list_item_1,assignments){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView =(TextView)super.getView(position, convertView, parent);
                textView.setText(assignments.get(position).getName());
                return  textView;
            }

            @Override
            public int getCount() {
                return assignments.size();
            }

            @Override
            public Assignment getItem(int position) {
                return assignments.get(position);
            }
        });
        assignmentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),AssignmentActivity.class);
                intent.putExtra("AssignmentInfo",assignments.get(position));
                startActivity(intent);
            }
        });
        assignmentList.setAdapter(adapter);
        return  rootview;

    }

    //function to get all assignment
    public void getAssignment(final String courseCode){
        net.getAssignment(courseCode,
                getActivity(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //notifications = new ArrayList<Notification>();
                        Log.d("Assignment_response", response.toString());
                        try {
                            JSONArray assignmentJSON = response.getJSONArray("assignments");
                            int i = 0;
                            while (i < assignmentJSON.length()) {
                                assignments.add(new Assignment(assignmentJSON.getJSONObject(i).getString("name"),assignmentJSON.getJSONObject(i).getString("created_at"),assignmentJSON.getJSONObject(i).getInt("late_days_allowed"),assignmentJSON.getJSONObject(i).getString("deadline"),assignmentJSON.getJSONObject(i).getString("description"),courseCode));
                                i++;
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network Error!!", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                });
    }
}

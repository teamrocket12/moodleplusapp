package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AssignmentActivity extends AppCompatActivity {

    NetworkFunctions net=new NetworkFunctions();
   Assignment assignment;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    String timeNow=(dateFormat.format(date)); //2013/10/15 16:16:39
    Date deadlineDate;


    TextView course,title,created,deadline,latedays,description,timenow,timeleft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment);

        Intent intent=getIntent();
        assignment=(Assignment)intent.getSerializableExtra("AssignmentInfo");
        setTitle(R.string.AssignmentTitle);
        course=(TextView) findViewById(R.id.assignment_course);
        course.setText((assignment.courseCode));
        title=(TextView) findViewById(R.id.assignment_title);
        title.setText((assignment.getName()));
        created=(TextView) findViewById(R.id.assignment_created_at);
        created.setText("Created At : "+(assignment.getCreatedAt()));
        deadline=(TextView) findViewById(R.id.assignment_deadline);
        deadline.setText("Deadline : " +(assignment.getDeadline()));
        latedays=(TextView) findViewById(R.id.assignment_lateDays);
        latedays.setText("Late Days Allowed : "+String.valueOf(assignment.getLateDaysAllowed()));


        try {
            deadlineDate = dateFormat.parse(assignment.deadline);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = (deadlineDate.getTime() - date.getTime());
        long days=diff / (24 * 60 * 60 * 1000);
        long hours=(diff%(24 * 60 * 60 * 1000))/(60 * 60 * 1000);
        if(days<0 || hours <0){
            timeleft=(TextView) findViewById(R.id.assignment_timeleft);
            timeleft.setText("Deadline Crossed!!");
        }
        else{
            timeleft=(TextView) findViewById(R.id.assignment_timeleft);
            timeleft.setText(String.valueOf(days)+" days and "+String.valueOf(hours)+" hours left");
        }
        timenow=(TextView) findViewById(R.id.timeNow);
        timenow.setText(timeNow);

        description=(TextView) findViewById(R.id.assignment_description);
        description.setText(Html.fromHtml(assignment.description));

    }

    public void setReminder(View view){

        Date startTime=new Date(deadlineDate.getTime()-43200000);
        Date endTime=deadlineDate;
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,endTime);
        intent.putExtra("allDay", false);
        intent.putExtra("title", assignment.courseCode.toUpperCase()+" : "+assignment.getName());
        intent.putExtra("description", "Submission Deadline");
        startActivity(intent);

    }



}

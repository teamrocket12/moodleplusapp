package com.cop.abhinav_kushagra.moodleplusapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity {

    TextView name,username,entrynumber,emailid;
    User user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        setTitle("User Info");
        Intent intent=getIntent();
        user=(User) intent.getSerializableExtra("userData");

        name=(TextView)findViewById(R.id.user_name);
        username=(TextView) findViewById(R.id.user_username);
        entrynumber=(TextView) findViewById(R.id.user_entryno);
        emailid=(TextView) findViewById(R.id.user_email);
        name.setText(user.firstName+" "+user.lastName);
        username.setText(user.username);
        entrynumber.setText(user.entryNumber);
        emailid.setText(user.email);
    }
}

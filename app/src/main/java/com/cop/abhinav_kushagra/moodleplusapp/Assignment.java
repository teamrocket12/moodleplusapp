package com.cop.abhinav_kushagra.moodleplusapp;

import java.io.Serializable;

public class Assignment implements Serializable {
    String name;
    String createdAt;
    int lateDaysAllowed;
    String deadline;
    String description;
    String courseCode;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getLateDaysAllowed() {
        return lateDaysAllowed;
    }

    public void setLateDaysAllowed(int lateDaysAllowed) {
        this.lateDaysAllowed = lateDaysAllowed;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }


    public Assignment(String name, String createdAt, int lateDaysAllowed, String deadline, String description, String courseCode) {
        this.name = name;
        this.createdAt = createdAt;
        this.lateDaysAllowed = lateDaysAllowed;
        this.deadline = deadline;
        this.description = description;
        this.courseCode = courseCode;
    }
}



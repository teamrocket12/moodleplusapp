package com.cop.abhinav_kushagra.moodleplusapp;

import java.io.Serializable;

public class DiscussionThread implements Serializable {
    int userID;
    String description;
    String title;
    String createdAt;
    int registeredCourseid;
    String updatedAt;
    int id;

    public DiscussionThread(int userID, String description, String title, String createdAt, int registeredCourseid, String updatedAt, int id) {
        this.userID = userID;
        this.description = description;
        this.title = title;
        this.createdAt = createdAt;
        this.registeredCourseid = registeredCourseid;
        this.updatedAt = updatedAt;
        this.id = id;
    }

    public DiscussionThread(int userID, String description, String title, int registeredCourseid, String updatedAt, int id) {
        this.userID = userID;
        this.description = description;
        this.title = title;
        this.registeredCourseid = registeredCourseid;
        this.updatedAt = updatedAt;
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRegisteredCourseid() {
        return registeredCourseid;
    }

    public void setRegisteredCourseid(int registeredCourseid) {
        this.registeredCourseid = registeredCourseid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}

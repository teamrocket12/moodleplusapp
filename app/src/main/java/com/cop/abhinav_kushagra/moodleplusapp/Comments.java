package com.cop.abhinav_kushagra.moodleplusapp;

/**
 * Created by kushagra on 2/23/2016.
 */
public class Comments {


    int userID;
    String description;
    String createdAt;
    String userName;
    String timediff;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Comments(int userID, String description, String createdAt, String userName) {
        this.userID = userID;
        this.description = description;
        this.createdAt = createdAt;
        this.userName = userName;
    }

    public Comments(int userID, String description, String createdAt) {
        this.userID = userID;
        this.description = description;
        this.createdAt = createdAt;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
